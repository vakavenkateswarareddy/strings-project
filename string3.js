function getMonth(givenString){
    let object={"1":"january","2":"febuary","3":"march","4":"april","5":"may","6":"june","7":"july","8":"august",
                "9":"september","10":"october","11":"november","12":"december"};
    if(typeof givenString==="string"){
        let newArray=givenString.split("/");
        let month=object[parseInt(newArray[0])];
        if(month===undefined){
            return ("month out of range")
        }else{
            return month
        }   
    }else{
        return " ";
    }
}
module.exports=getMonth;