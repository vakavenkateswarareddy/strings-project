function fullName(object){
    let fullName=""
    for (let key in object){
        let value=object[key];
        let newValue=(value[0]).toUpperCase()+(value.slice(1)).toLowerCase()
        fullName=fullName+newValue+" ";
    }
    return fullName;
};

module.exports=fullName;