function convertingArrToString(array){
    if (Array.isArray(array) && array.length!=0){
        let newString="";
        for (let i=0;i<array.length;i++){
            let attachedString=array[i]+" ";
            newString=newString.concat(attachedString);
        }
        return newString;
    }else{
        return "";
    }
}

module.exports=convertingArrToString;