function convertingStringToNumber(string1){
    if(typeof string1==="string"){
        let newString=string1.split(".");
        let newArray=[];
        for (let i=0;i<newString.length;i++){
            if(isNaN(newString[i])){
                return []

            }else{
                newArray.push(parseInt(newString[i]));
            }
            
        }
        return newArray;
    }
    else{
        return [];
    }
};

module.exports=convertingStringToNumber;

// "111.139.161.143"

